﻿/* 
  consultas de accion 2
*/

  -- ejercicio 4
  
  -- crear el campo
  ALTER TABLE productos 
    DROP COLUMN descuento1;

  ALTER TABLE productos 
     ADD COLUMN descuento1 float DEFAULT NULL;

  -- accion
  -- 2 update
  UPDATE productos
    SET descuento1=importe_base*0.1
    WHERE rubro="verduras";
  
  UPDATE productos
    SET descuento1=0
    WHERE NOT rubro="verduras";

  -- 1 update
-- opcion1 
UPDATE productos p
  SET descuento1=IF(p.rubro="verduras",p.importe_base*0.1,0);
-- opcion2
UPDATE productos p
  SET descuento1=p.importe_base*IF(p.rubro="verduras",0.1,0);


-- ejercicio 5

-- crear el campo 
  ALTER TABLE productos ADD COLUMN descuento2 float DEFAULT 0;

-- con 2 update
  UPDATE productos 
    set descuento2=importe_base*0.2
    WHERE `u/medida`='atado';

  UPDATE productos 
    set descuento2=importe_base*0.05
    WHERE NOT `u/medida`='atado';

  -- con 1 update
UPDATE productos
  SET descuento2=importe_base*IF(`u/medida`='atado',0.2,0.05);

-- ejercicio 6

  ALTER TABLE productos ADD COLUMN descuento3 float DEFAULT 0;

-- con 2 update

  UPDATE productos p 
    SET p.descuento3=p.importe_base*0.2
    WHERE p.rubro='frutas' AND p.importe_base>15;

  UPDATE productos p 
    SET p.descuento3=0
    WHERE NOT (p.rubro='frutas' AND p.importe_base>15);

  -- con 1 update 
  UPDATE productos p
      SET p.descuento3=p.importe_base*
      IF(p.rubro='frutas' AND p.importe_base>15,0.2,0);


  -- ejercicio 7

    ALTER TABLE productos ADD COLUMN descuento4 float DEFAULT 0;

    -- con 2 update
      /*UPDATE productos p JOIN 
        (SELECT * FROM productos WHERE granja='primavera' OR granja='litoral') c1
        USING(producto) 
        set p.descuento4=p.importe_base*0.5;*/

      UPDATE productos p
        SET p.descuento4=p.importe_base*0.5
        WHERE granja='primavera' OR granja='litoral';

      UPDATE productos p
        SET p.descuento4=p.importe_base*0.25
        WHERE NOT(granja='primavera' OR granja='litoral');

      -- 1 update
      UPDATE productos p 
        SET p.descuento4=p.importe_base*
        IF(granja='primavera' OR granja='litoral',0.5,0.25);

-- ejercicio 8

  ALTER TABLE productos ADD COLUMN aumento1 float DEFAULT 0;

  -- 2 update
    UPDATE productos p 
      set aumento1=p.importe_base*0.1
      WHERE 
        (p.rubro="frutas" OR p.rubro="verduras") 
          AND 
        (granja="la garota" OR granja="la pocha");

    UPDATE productos p 
      set aumento1=0
      WHERE 
      NOT(
          (p.rubro="frutas" OR p.rubro="verduras") 
            AND 
          (granja="la garota" OR granja="la pocha")
        );


  -- 1 update
    UPDATE productos p
      SET p.aumento1=p.importe_base*
      IF((p.rubro="frutas" OR p.rubro="verduras") 
          AND 
        (granja="la garota" OR granja="la pocha"),0.1,0);

  -- ejercicio 9

ALTER TABLE productos ADD COLUMN presentacion int DEFAULT 0;

-- con varios update
  UPDATE productos p 
    SET p.presentacion=1
    WHERE p.`u/medida`='atado';
  
  UPDATE productos p 
    SET p.presentacion=2
    WHERE p.`u/medida`='unidad';

  UPDATE productos p 
    SET p.presentacion=3
    WHERE p.`u/medida`='kilo';

  -- con 1 update
    UPDATE productos p
      SET presentacion=
      IF(p.`u/medida`='atado',1,IF(p.`u/medida`='unidad',2,3));

    UPDATE productos p
       SET presentacion=
          CASE
            WHEN p.`u/medida`='atado' THEN 1
            WHEN p.`u/medida`='unidad' THEN 2
            WHEN p.`u/medida`='kilo' THEN 3
          END;

    UPDATE productos p
       SET presentacion=
          CASE
            WHEN p.`u/medida`='atado' THEN 1
            WHEN p.`u/medida`='unidad' THEN 2
            ELSE 3
          END;

    -- ejercicio 10

ALTER TABLE productos ADD COLUMN categoria char(1) DEFAULT NULL;

-- con varios update
  UPDATE productos p
    set categoria='A'
    WHERE p.importe_base<10;
  
  UPDATE productos p
    set categoria='B'
    WHERE p.importe_base BETWEEN 10 AND 20;

  UPDATE productos p
    set categoria='C'
    WHERE p.importe_base>20;

  -- con if
  UPDATE productos p
    set categoria=
    IF(p.importe_base<10,'A',IF(p.importe_base BETWEEN 10 AND 20,'B','C'));

  UPDATE productos p
    set categoria=
    IF(p.importe_base<10,'A',IF(p.importe_base<=20,'B','C'));

UPDATE productos p
    set categoria=
      CASE
        WHEN p.importe_base<10 THEN 'A'
        WHEN p.importe_base BETWEEN 10 AND 20 THEN 'B'
        ELSE 'C'
      END;

UPDATE productos p
    set categoria=
      CASE
        WHEN p.importe_base<10 THEN 'A' 
        WHEN p.importe_base<=20 THEN 'B' 
        ELSE 'C'
      END;
-- ejercicio 11
  ALTER TABLE productos ADD COLUMN aumento2 float DEFAULT 0;

  -- case
    UPDATE productos p
      SET aumento2=p.importe_base*
        CASE 
          WHEN p.producto='frutas' AND p.granja='litoral' THEN 0.1
          WHEN p.producto='verduras' AND p.granja='el ceibal' THEN 0.15
          WHEN p.producto='semillas' AND p.granja='el canuto' THEN 0.2
          ELSE 0
        END;
  -- if
     UPDATE productos p
      SET aumento2=p.importe_base*
        IF(p.producto='frutas' AND p.granja='litoral',0.1,
          IF(p.producto='verduras' AND p.granja='el ceibal',0.15,
            IF(p.producto='semillas' AND p.granja='el canuto',0.2,0)));








  
   

